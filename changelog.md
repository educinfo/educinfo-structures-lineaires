# Changelog
*educinfo-structures-lineaires*

Package pour l'apprentissage des structures linéraires de liste,pile et file (Tale)

Tous les changements notables seront documentés dans ce fichier

Le format s'appuie sur [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
et ce projet adhère au [versionnement sémantique](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.0]

### Ajout
- ce changelog
- structure des répertoires
- page.php avec le sommaire
- ajout des pages introduction