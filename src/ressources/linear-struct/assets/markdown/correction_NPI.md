Voici la fonction qui permet de faire une calculatrice NPI qui répond au problème :

On notera les tests nombreux pour repérer les erreurs :

```python
def calcule(expression):
    valeur = None
    tableau = expression.split()
    ma_pile = pile()
    ### Mettez votre code ici
    for symbole in tableau :
        # Si c'est un nombre
        if symbole not in ('+','*','/','-','**'):
            ma_pile.empile(symbole)
        else :
            if ma_pile.estVide() :
               return None
            n1 = float(ma_pile.depile())
            if ma_pile.estVide() :
               return None
            n2 = float(ma_pile.depile())
            if symbole == '+' :
                ma_pile.empile(n2+n1)
            elif symbole == '-':
                ma_pile.empile(n2-n1)
            elif symbole == '/':
                ma_pile.empile(n2/n1)
            elif symbole == '*':
                ma_pile.empile(n2*n1)
            elif symbole == '**':
                ma_pile.empile(n2**n1)
            else :
                return None # symbole interdit ?
    if ma_pile.estVide() :
        return None
    valeur = ma_pile.depile()
    if ma_pile.estVide() : 
        return valeur
    else :
        return None
```