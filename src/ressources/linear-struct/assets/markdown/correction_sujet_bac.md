### Question 1.a

![question 1 a ](ressources/linear-struct/assets/img/question_1a.png)

La pile sera :

<table style="border-top: 0px ; border-left : 1px solid black; border-right: 1px solid black; border-bottom : 1px solid black; text-align:center;">
<tr><td style="padding-left:1em;padding-right:1em;"><td><tr>
<tr><td style="padding-left:1em;padding-right:1em;">rouge<td><tr>
<tr><td style="padding-left:1em;padding-right:1em;">vert<td><tr>
<tr><td style="padding-left:1em;padding-right:1em;">jaune<td><tr>
<tr><td style="padding-left:1em;padding-right:1em;">rouge<td><tr>
<tr><td style="padding-left:1em;padding-right:1em;">jaune<td><tr>
</table>

<br>

### Question 1.b

![question 1 a ](ressources/linear-struct/assets/img/question_1b.png)


```python
def taille_file(F):
    taille = 0
    file_temp = creer_file_vide()
    while not est_vide(F):
        taille = taille + 1
        enfiler(file_temp, defiler(F))
    while not est_vide(file_temp):
        enfiler(F, defiler(file_temp))
    return taille
```

### Question 2

![question 1 a ](ressources/linear-struct/assets/img/question_2.png)

Utiliser une pile unique inverserait l'ordre des éléments. En utilisant deux piles,on contourne cette difficulté

```python
def former_pile(F):
    pile_finale = creer_pile_vide()
    pile_temp = creer_pile_vide()
    while not est_vide(F):
        empiler(pile_temp,defiler(F))
    while not est_vide(pile_temp):
        empiler(pile_finale,depiler(pile_temp))
    return pile_finale
```

### Question 3

![question 1 a ](ressources/linear-struct/assets/img/question_3.png)


On utilise le même principe que dans la question 1, en comptant en plus les occurences d'un élément

```python
def nb_elements(F, ele):
    nb = 0
    file_temp = creer_file_vide()
    while not est_vide(F):
        x = defiler(F)
        if x==ele:
            nb = nb + 1
        enfiler(file_temp, x)
    while not est_vide(file_temp):
        enfiler(F, defiler(file_temp))
    return nb
```

### Question 4

![question 1 a ](ressources/linear-struct/assets/img/question_4.png)


En écrivant ainsi la fonction **verifier\_contenu**, on va appeler 3 fois la fonction **nb\_elements**. On peut aussi écrire pour ne parcourir qu'une seule fois la liste et
pas trois fois.

```python
def verifier_contenu(F, nb_rouge, nb_vert, nb_jaune):
    return nb_elements(F, "rouge") <= nb_rouge and nb_elements(F,"vert") <= nb_vert and nb_elements(F, "jaune") <= nb_jaune
```