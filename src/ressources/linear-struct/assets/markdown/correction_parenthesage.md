
Voici un code possible pour la fonction qui permet de vérifier qu'une expression est bien parenthésée.

```python
def bonneParentheses(expression):
    ma_pile = pile()
    for symbole in expression :
        if symbole in ('(','{','['):
            ma_pile.empile(symbole)
        elif symbole in (')','}',']'):
            if ma_pile.estVide() :
                return False
            ouvrant = ma_pile.depile()
            if ouvrant == '(' and symbole !=')':
                return False
            if ouvrant == '{' and symbole !='}':
                return False
            if ouvrant == '[' and symbole !=']':
                return False
    if not ma_pile.estVide() :
        return False
    return True
```