class cellule:
    """ Classe qui modélise une cellule dans une structure linéaire """
    def __init__(self,element):
        self.contenu = element
        self.suivant = None

class pile:
    """ Classe qui implémente une pile"""
    def __init__(self):
        self.sommet = None

    def estVide(self):
        return self.sommet == None

    def empile(self,element):
        nouveau = cellule(element)
        nouveau.suivant = self.sommet
        self.sommet = nouveau

    def depile(self):
        valeur = self.sommet.contenu
        self.sommet = self.sommet.suivant
        return valeur

    def affiche(self):
        pointeur = self.sommet
        while pointeur != None :
            print(pointeur.contenu)
            pointeur = pointeur.suivant

###
# Complétez les méthodes de la classe pileTableau
###

class pileTableau:
    """ Classe qui implémente une pile dans un tableau"""
    def __init__(self):
        self.T = []

    def estVide(self):
        pass

    def empile(self,element):
        pass

    def depile(self):
        pass

    def affiche(self):
        for i in range(len(self.T)):
            print(self.T[-i-1])
        
######
# Exécutez le code de test
######

p1 = pile()
p2 = pile()

p1.empile(3)
p1.empile(4)
p1.empile(5)
print("P1 : ")
p1.affiche()
print("P2 : ")
p2.affiche()
while not p1.estVide():
    p2.empile(p1.depile())
    print("P1 : ")
    p1.affiche()
    print("P2 : ")
    p2.affiche()
