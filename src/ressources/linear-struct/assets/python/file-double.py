# File python codée avec une liste doublement chainée
import time
class Noeud:
    def __init__(self, data):
        self.valeur = data
        self.suivant = None
        self.precedent = None

class FileDouble:
    def __init__(self):
        self.tete = None
        self.queue = None

    def estVide(self):
        return self.tete is None

    def enfile(self, valeur):
        nouveau_noeud = Noeud(valeur)
        if self.estVide():
            self.tete = nouveau_noeud
            self.queue = nouveau_noeud
        else:
            self.queue.suivant = nouveau_noeud
            nouveau_noeud.precedent = self.queue
            self.queue = nouveau_noeud

    def defile(self):
        valeur = self.tete.valeur
        self.tete = self.tete.suivant
        if self.tete is not None:
            self.tete.precedent = None
        else:
            self.queue = None
        return valeur
    
class FileTableau:
    def __init__(self):
        self.tableau = []
    
    def estVide(self):
        return len(self.tableau) == 0

    def enfile(self, valeur):
        self.tableau.insert(0,valeur)
    
    def defile(self):
        return self.tableau.pop()

# Comparaison de performance
# Test de performance

f1=FileTableau()
f2=FileDouble()

for nbOperations in (1000,10000,100000):
    for test,nom in [(f1,"un tableau"),(f2,"une liste doublement chainée")] :
        debut = time.time()
        for i in range(nbOperations):
            test.enfile(i)
        for i in range(nbOperations) :
            test.defile()
        fin = time.time()
        print(f"Temps écoulé pendant le test de file avec {nom} pour {nbOperations} opérations : {1000*(fin-debut):.2f} millisecondes")
    print()