#!/usr/bin/python3
import time

class FileTableau:
    def __init__(self):
        self.T = []

    def estVide(self):
        return len(self.T)==0

    def enfile(self,element):
        self.T.insert(0,element)

    def defile(self):
        return self.T.pop()

class File2Piles:
    def __init__(self):
        self.entree = []
        self.sortie = []

    def estVide(self):
        return len(self.entree)==0 and len(self.sortie)==0

    def enfile(self,element):
        self.entree.append(element)

    def defile(self):
        if len(self.sortie)==0 :
            for i in range(len(self.entree)):
                self.sortie.append(self.entree.pop())
        return self.sortie.pop()

# Test de performance

f1=FileTableau()
f2=File2Piles()

for nbOperations in (10000,50000,100000):
    for test,nom in [(f1,"un tableau"),(f2,"2 piles")] :
        debut = time.time()
        for i in range(nbOperations):
            test.enfile(i)
        for i in range(nbOperations) :
            test.defile()
        fin = time.time()
        print(f"Temps écoulé pendant le test de file avec {nom} pour {nbOperations} opérations : {1000*(fin-debut):.2f} millisecondes")
    print()