class cellule:
    """ Classe qui modélise une cellule dans une structure linéaire """
    def __init__(self,element):
        self.contenu = element
        self.suivant = None

class pile:
    """ Classe qui implémente une pile"""
    def __init__(self):
        self.sommet = None

    def estVide(self):
        return self.sommet == None

    def empile(self,element):
        nouveau = cellule(element)
        nouveau.suivant = self.sommet
        self.sommet = nouveau

    def depile(self):
        valeur = self.sommet.contenu
        self.sommet = self.sommet.suivant
        return valeur

    def affiche(self):
        pointeur = self.sommet
        while pointeur != None :
            print(pointeur.contenu)
            pointeur = pointeur.suivant

def bonneParentheses(expression):
    ### Mettez votre code ici
    pass

assert bonneParentheses('(oui)')==True, "erreur sur '(oui)'"
assert bonneParentheses('{(oups})')==False, "erreur sur '{(oups})'"
assert bonneParentheses('[oui]{et encore[oui(je crois)]}') == True, "erreur sur '[oui]{et encore[oui(je crois)]}'"
