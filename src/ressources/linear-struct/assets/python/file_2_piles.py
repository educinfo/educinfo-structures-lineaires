#!/usr/bin/python3

###
# Complétez la classe ci dessous
###
class File2Piles:
    def __init__(self):
        self.entree = []
        self.sortie = []

    def estVide(self):
        pass

    def enfile(self,element):
        pass

    def defile(self):
        pass

### 
# tests
###
maFile = File2Piles()
for i in range(3) : 
    maFile.enfile(i)
T=[]
# La file est pleine
T.append(maFile.estVide())
# On vide la file dans le tableau
for i in range(3):
    T.append(maFile.defile())
# La file doit être vide
T.append(maFile.estVide())

assert T == [False,0,1,2,True], "erreur sur la file avec 2 Tableaux"