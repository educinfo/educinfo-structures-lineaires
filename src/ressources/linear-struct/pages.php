<?php
/*****************
Terminale NSI
Structures linéaires 
******************/
global $pages;

// Bases du javascript
$menu_activite = array(
    "titre" => "Structures linéaires",
    "contenu" => [
        '01_introduction',
        '02_pile',
        '02.1_notion_pile',
        '03_parentheses',
        '04_NPI',
        '05_pile_tableau',
        '07_file',
        '08_file_chainee',
        '09_file_2_piles'
    ]
);

// pages des bases du javascript
$linear_struct_pages = array(
    '01_introduction' => array(
        "template" => 'linear-struct/01_introduction.twig.html',
        "menu" => 'linear-struct',
        'page_precedente' => NULL,
        'page_suivante' => '02_pile',
        'titre' => 'Introduction',
        "css" => 'ressources/linear-struct/assets/css/linear-struct.css'
    ),
    '02_pile' => array(
        "template" => 'linear-struct/02_pile.twig.html',
        "menu" => 'linear-struct',
        'page_precedente' => '01_introduction',
        'page_suivante' => '02.1_notion_pile',
        'titre' => 'La pile par l\'exemple',
        "css" => 'ressources/linear-struct/assets/css/linear-struct.css'
    ),
    '02.1_notion_pile' => array(
        "template" => 'linear-struct/02.1_notion_pile.twig.html',
        "menu" => 'linear-struct',
        'page_precedente' => '02_pile',
        'page_suivante' => '03_parentheses',
        'titre' => 'Notion de pile',
        "css" => 'ressources/linear-struct/assets/css/linear-struct.css'
    ),
    '03_parentheses' => array(
        "template" => 'linear-struct/03_parentheses.twig.html',
        "menu" => 'linear-struct',
        'page_precedente' => '02.1_notion_pile',
        'page_suivante' => '04_NPI',
        'titre' => 'Bon parenthésage',
        "css" => 'ressources/linear-struct/assets/css/linear-struct.css'
    ),
    'CC398123_parentheses' => array(
        "template" => 'linear-struct/03_parentheses.correction.twig.html',
        "menu" => 'linear-struct',
        'page_precedente' => NULL,
        'page_suivante' => NULL,
        'titre' => 'Bon parenthésage',
        "css" => 'ressources/linear-struct/assets/css/linear-struct.css'
    ),
    '04_NPI' => array(
        "template" => 'linear-struct/04_NPI.twig.html',
        "menu" => 'linear-struct',
        'page_precedente' => '03_parentheses',
        'page_suivante' => '05_pile_tableau',
        'titre' => 'Calculatrice NPI',
        "css" => 'ressources/linear-struct/assets/css/linear-struct.css'
    ),
    'CCPO456BHY_NPI' => array(
        "template" => 'linear-struct/04_NPI.correction.twig.html',
        "menu" => 'linear-struct',
        'page_precedente' => null,
        'page_suivante' => null,
        'titre' => 'Correction calculatrice NPI',
        "css" => 'ressources/linear-struct/assets/css/linear-struct.css'
    ),
    '05_pile_tableau' => array(
        "template" => 'linear-struct/05_pile_tableau.twig.html',
        "menu" => 'linear-struct',
        'page_precedente' => '04_NPI',
        'page_suivante' => '07_file',
        'titre' => 'Une pile dans un tableau',
        "css" => 'ressources/linear-struct/assets/css/linear-struct.css'
    ),/*
    '06_calc' => array(
        "template" => 'linear-struct/06_calc.twig.html',
        "menu" => 'linear-struct',
        'page_precedente' => '05_pile_tableau',
        'page_suivante' => '07_file',
        'titre' => 'Calculatrice',
        "css" => 'ressources/linear-struct/assets/css/linear-struct.css'
    ),*/
    '07_file' => array(
        "template" => 'linear-struct/07_file.twig.html',
        "menu" => 'linear-struct',
        'page_precedente' => '05_pile_tableau',
        'page_suivante' => '08_file_chainee',
        'titre' => 'Structure de file',
        "css" => 'ressources/linear-struct/assets/css/linear-struct.css'
    ),
    '08_file_chainee' => array(
        "template" => 'linear-struct/08_file_chainee.twig.html',
        "menu" => 'linear-struct',
        'page_precedente' => '07_file',
        'page_suivante' => '09_file_2_piles',
        'titre' => 'File avec une liste doublement chaînée',
        "css" => 'ressources/linear-struct/assets/css/linear-struct.css'
    ),
    '09_file_2_piles' => array(
        "template" => 'linear-struct/09_file_2_piles.twig.html',
        "menu" => 'linear-struct',
        'page_precedente' => '08_file_chainee',
        'page_suivante' => NULL,
        'titre' => 'File avec deux piles',
        "css" => 'ressources/linear-struct/assets/css/linear-struct.css'
    ),
    '10_liste_abstraite' => array(
        "template" => 'linear-struct/10_liste_abstraite.twig.html',
        "menu" => 'linear-struct',
        'page_precedente' => '01_introduction',
        'page_suivante' => '02.1_notion_pile',
        'titre' => 'Structure abstraite de liste',
        "css" => 'ressources/linear-struct/assets/css/linear-struct.css'
    ),
    '11_grille_notation' => array(
        "template" => 'linear-struct/11_grille_notation.twig.html',
        "menu" => 'linear-struct',
        'page_precedente' => null,
        'page_suivante' => null,
        'titre' => 'Grille de notation du projet',
        "css" => 'ressources/linear-struct/assets/css/linear-struct.css'
    ),
    'CC765h890_bac' => array(
        "template" => 'linear-struct/CC_bac.twig.html',
        "menu" => 'linear-struct',
        'page_precedente' => null,
        'page_suivante' => null,
        'titre' => 'Correction du sujet type BAC',
        "css" => 'ressources/linear-struct/assets/css/linear-struct.css'
    ),
);

$pages = array_merge($pages, $linear_struct_pages);

