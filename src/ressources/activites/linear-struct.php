<?php

register_activity(
    'linear-struct',
    array(
        'category' => 'NSIT',
        'section' => 'NSITtypeconstruit',
        'type' => 'url',
        'titre' => "Structures linéaires : listes, piles et files",
        'auteur' => "Laurent COOPER",
        'URL' => 'index.php?activite=linear-struct&page=01_introduction',
        'commentaire' => "Les listes, les piles et les files sont des structures de données usuelles très utiles pour résoudre des problèmes variés",
        'directory' => 'linear-struct',
        'icon' => 'fas fa-cogs',
        'prerequis' => "tableaux"
    )
);